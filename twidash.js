const Discord = require('discord.js');
const client = new Discord.Client();
const dinky = require('dinky.js');

const config = require('./config.json');
//Get the api token from a separate file so we can keep it out of version control
const token = require('./token.json');

client.on('ready', async () => {
    console.log('Connected as ' + client.user.tag);
    let server = client.guilds.find(guild => guild.name === 'test');
    getPicsChannels();

    const img = await dinky().search(config.tags).minScore(10).random().then((pic) => pic);
    const post = makePost() + '\nhttp:' + removeTags(img.image);
    for(let channel of getPicsChannels()){
        if(channel.type === "text"){
            channel.send(post);
        }
    };

    client.destroy();
});

//Figured out what to remove by eyeballing what comes back from derpibooru's api.
//Keep an eye on this.
function removeTags(str){
    const start = str.indexOf('__');
    const end = str.lastIndexOf('.');
    const newStr = str.substring(0, start) + str.substring(end);
    return newStr
}


function makePost(){
    if(process.argv.length > 2){
        return process.argv[2];
    }
    const messages = require('./messages.json');
    let text = getRandomItem(messages.messages);
    return text;
}

function getPicsChannels(){
    pics = []
    for(let server of config.servers){
        const guild = client.guilds.find(g => g.name === server.name);
        pics.push(guild.channels.find(ch => ch.name === server.channel));
    }
    return pics;
}

//Grab a random element from an array
function getRandomItem(arr){
    let rand = Math.floor(Math.random() * (arr.length - 1));
    return arr[rand];
}

client.login(token.token);


