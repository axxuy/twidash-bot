TwiDash Bot posts a message in all channels named "pony-pics" in all servers it is in. The message has two parts: 1) a short piece of text about why TwiDash is the best ship 2) a url to a TwiDash image.

There are two main configuration files, `messages.json` which contains the text messages, and `config.json` contains other settings.

`messages.json` contains a single field, `messages`, which is an array of strings containing the messages to be posted.

`config.json` contains the following fields: `tags`, an array of tags used to search derpibooru.org for images. Check that website for avaialable tags. `servers` contains a list of `name`s of servers and `channel`s on those servers to post in.

You will also need to place your API token in a file named `token.json`, which should contain the following:
```
{
    "token": "your-token-here"
}
```
