import json
import random
import logging
import discord
from derpibooru import Search, sort

client = discord.Client()
c = open("/home/axxuy/twidash-bot/config.json", "r")
config = json.load(c)
logging.basicConfig(filename=config["log"], level=logging.DEBUG, format="%(asctime)s: %(message)s")

def get_message():
    with open("/home/axxuy/twidash-bot/messages.json", "r") as f:
        messages = json.load(f)

        tags = config["tags"]
        mesg = random.choice(messages["messages"])

        #1% chance of LyraBon mode
        if random.random() < 0.01:
            logging.info("Posting in LyraBon mode")
            tags = config["lb_tags"]
            mesg = random.choice(messages["lb_messages"])
        else:
            logging.info("Posting in TwiDash mode")

        search = Search().sort_by(sort.RANDOM).query(*tags).limit(2)
        return mesg + "\n" + next(search).representations["large"]

def find(pred, iter):
    for i in iter:
        if pred(i):
            return i
    return None

def get_channels():
    channels = []
    for server in config["servers"]:
        guild = find(lambda g: str(g) == server["name"], client.guilds)
        channels.append(find(lambda ch: str(ch) == server["channel"], guild.channels))
    return channels

@client.event
async def on_ready():
    logging.info("Connected as %s", client.user)
    message = get_message()
    channels = get_channels()
    for chan in channels:
        logging.info("Sending to %s", str(chan))
        await chan.send(message)
    logging.info("All messages sent")
    await client.close()


token = json.load(open("/home/axxuy/twidash-bot/token.json", "r"))
client.run(token["token"])
